#+options: toc:nil num:nil author:nil

* ncdu command


~ncdu~ command is a great tool to check disk space usage. There are multiple
options to do this task in GNU-linux systems.

~ncdu~ is quite handy: simple, beatiful and let the users to interact with the files
directly in the command line, to explore recursively and move between directories.

Installing in Parabola GNU-linux:

#+begin_src bash
  pacman -S ncdu
#+end_src

To use, just call the program and await for the outocome:

#+begin_src bash
  ncdu
#+end_src
